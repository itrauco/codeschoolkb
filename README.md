CodeSchool Kb
A knowledge base of Apple-support inspired, self-help styled technical support documents for code school student developers.

Status: In Active Development

Announced: 2019-03-19
Demo Date: 2019-06-04

Scope: 
Industry-Wide

Objectives:
1. Establish CodeSchool Kb as a technical resource knowledge base, not a blog, used by thousands of code school student developers 

2. Establish CodeSchool Kb as "ITIL" for the student body of for-profit code schools

3. Establish an ITSM self-service strategy focused knowledge base for optmizing the "Student Developer Experience" through system and process standardization that removes barriers to dev student success brought on by the absence of formalized IT policies for the student body of a for-profit code schools/bootcamp. 

4. Apply IT Systems Analysis and Design(SAD) processes to document and model the software/application development life cycle with a focus on object-oriented data modeling techniques and the wonders of UML, e.g. use-case diagramming, object modeling, class modeling, schemas. 

5. Introduce student developers to the logic and workflows of common enterprise IT management practices, e.g. password managers, ticket management/triage, technical writing/documentation, case escalation, styling, naming conventions, security and compliance, UML, workbreak down structures, technology governance and continuity strategies, technical support, troubleshooting process, probing process, text expanders, wetc. 

Exit Strategies/Outcomes:
1. This idea evolves into an enterprise focused on being the "IT" of the student body for code schools. Students are customers of the code school, a for-profit business with SLAs that provides technical support to their student body.

2. Project develops a large user base, many code schools adopt the outlined recommendations, and a community movement develops to maintain and progress it. It's like ITIL for the student body of the code school industry.

3. Project will be used as my final capstone piece and handover ready, but is never picked up and permanently shelfed.

Areas of  Focus:
1. Achieving longterm self-sustainability and scalability of a system with thousands of active end users that evolves over time

2. techno-babble free writing

3. Maintaining an evidence-based practice approach perspective

4. Object oriented data modeling

5. Software Development Life Cycle

6. Scalability 

7. SE project planning

8. Enhancing industry reputation/digital footprint 